module.exports = class kick {
    constructor(){
        this.name = 'kick',
        this.alias = ['k'],
        this.usage = '?kick'
    }
    
    run(bot, message, args){
        const member = message.mentions.members.first();

        if (!member){
        return message.reply(
            "Who are you trying to kick? You must @ mention someone!"
            );
        }

        if (!member.kickable) {
            return message.reply(`I can't kick this user. Sorry!`);
        }

        return member
        .kick()
        .then(() => message.reply(`${member.user} was sadly kicked.`))
        .catch(error => message.reply(`Sorry an error occured`));

    return;
    }
}