const Discord = require("discord.js");
module.exports = class drink {
    constructor(){
        this.name = 'drink',
        this.alias = ['d'],
        this.usage = '?drink'
    }
    
    run(bot, message, args){
        const potion_ids = ["empty","water","mundane","thick","awkward","night_vision","long_night_vision","invisibility","long_invisibility","leaping","strong_leaping","long_leaping","fire_resistance","long_fire_resistance","swiftness","strong_swiftness","long_swiftness","slowness","strong_slowness","long_slowness","water_breathing","long_water_breathing","healing","strong_healing","harming","strong_harming","poison","strong_poison","long_poison","regeneration","strong_regeneration","long_regeneration","strength","strong_strength","long_strength","weakness","long_weakness","luck","turtle_master","strong_turtle_master","long_turtle_master","slow_falling","long_slow_falling"];
        const wine_names = ["Ace of Spades (Armand de Brignac by Cattier)","Angélus (Château), Saint-Emilion Grand Cru","Astralis Shiraz (Clarendon Hills)","Ausone (Château) Saint-Emilion Grand Cru","Beaucastel (Château)","Beaux Freres ‘The Beaux Freres Vineyard’ Pinot Noir","Belle Epoque (Perrier-Jouet)","Beaulieu Vineyard BV Georges de Latour Private Reserve Cabernet Sauvignon","Billecart-Salmon","Bin 707 Cabernet Sauvignon","Calon-Ségur (Château)","Canon (Château) Saint-Emilion Grand Cru","Cask 23 (Stag’s Leap Wine Cellars)","Caymus Vineyards Cabernet Sauvignon","Casillero del Diablo Reserva Cabernet Sauvignon","Chambertin Grand Cru","Charles Heidsieck Blanc des Millénaires","Cheval Blanc (Château)","Cloudy Bay Sauvignon Blanc","Corton-Charlemagne","Cristal (Louis Roederer)","Cos d’Estournel (Château)","Côte-Rôtie Brune et Blonde (E. Guigal)","Dom Pérignon","Dominus (Estate by Christian Moueix)","Ducru-Beaucaillou (Château)","L’Evangile (Château)","Échezeaux Grand Cru","Figeac (Château)","Gaja Barbaresco DOCG","Grange Bin 95 (Penfolds)","Gran Reserva 904 (La Rioja Alta)","Haut-Brion (Château)","Harlan Estate","Hermitage La Chapelle (Paul Jaboulet Ainé)","Hill of Grace Shiraz (Henschke)","Insignia (Joseph Phelps Vineyards)","Imperial Reserva (Compania Vinicola del Norte de Espana CVNE)","Jordan Winery Cabernet Sauvignon","Klein Constantia Vin de Constance","Krug","Lafite Rothschild (Château)","La Mission Haut-Brion (Château)","Latour (Château)","Léoville Barton (Château)","Léoville-Las Cases (Château)","Léoville Poyferré (Château)","Lynch-Bages (Château)","Dr. L Riesling (Loosen Bros)","Margaux (Château)","Masseto","Mateus The Original Rosé","Marques de Riscal Reserva","Mas de Daumas Gassac","Meiomi Pinot Noir","Miraval (Château) Côtes de Provence Rosé","Monte Bello (Ridge Vineyards)","Moët & Chandon","Montelena Chardonnay (Château)","Montrose (Château)","Mouton Rothschild (Château)","Musigny Grand Cru","Numanthia Termanthia","Opus One, Napa Valley","Ornellaia Bolgheri Superiore","Palmer (Château)","Pavie (Château)","Pédesclaux (Château)","Pétrus, Pomerol","Pichon Longueville Comtesse de Lalande (Château)","Pingus (Dominio)","Pol Roger, Champagne","Pontet-Canet (Château)","Quinta do Noval ‘Nacional’ Vintage Port","Quintessa Red, Rutherford","Rayas (Château)","Richebourg Grand Cru","Romanée-Conti Grand Cru","Romanée-Saint-Vivant Grand Cru","Rubicon (Meerlust Estate)","Sassicaia Bolgheri (Tenuta San Guido)","Salon Cuvée S Le Mesnil","Scharzhofberger Riesling (Egon Muller & other wineries)","Screaming Eagle Cabernet Sauvignon","Silver Oak Cellars Cabernet Sauvignon","Shafer Vineyards Hillside Select Cabernet Sauvignon","Solaia (Marchese Antinori)","La Tâche Grand Cru Monopole","Taittinger, Champagne","Talbot (Château)","Tignanello (Marchesi Antinori)","Tinto Pesquera Crianza (Bodegas Alejandro Fernandez)","Unico Gran Reserva (Bodegas Vega Sicilia)","Veuve Clicquot","Vieux Chateau Certan","Wehlener Sonnenuhr Riesling","Wine names starting with an X seem extremely rare","Yellow Tail","d’Yquem (Château)","Zind-Humbrecht Riesling Rangen de Thann Clos Saint Urbain (Domaine)","Zenato Amarone della Valpolicella Classico DOCG","Holden's special brew","Water","Hot Choclate","Woah, I want to die!"];
        
        var potion = potion_ids[Math.floor(Math.random()*potion_ids.length)];
        var name = wine_names[Math.floor(Math.random()*wine_names.length)];
        var command = `give @s minecraft:potion{Potion:"minecraft:${potion}",display:{Name:"{\\"text\\":\\"${name}\\"}"}}`;

        let wineEmbed = new Discord.RichEmbed()
        .setTitle("Wine")
        .setColor("#0cff00")
        .addField("Here you go!", `${command}`)
        .setFooter("Be safe!")

        
        
        return message.channel.send(wineEmbed);
    }
}