const Discord = require('discord.js');
const client = new Discord.Client();
const bot = new Discord.Client();
const token = require('./token.json');
const { CommandHandler } = require('djs-commands');
const fs = require ("fs");
const CH = new CommandHandler({
    folder: __dirname + "/commands/",
    prefix: ['?']
}); 

bot.on("ready", () => {
    console.log(bot.user.username + " is online.")
})

bot.on("message", (message) => {
    if(message.channel.type === 'dm') return;
    if(message.author.type === 'bot') return;
    let args = message.content.split(" ");
    let command = args[0];
    client.msgs = require ("./msg.json")
    let cmd = CH.getCommand(command);
    if(!cmd) return;

    try{
        cmd.run(bot, message, args);
    }catch(e){
        console.log(e)
    }

})


bot.login(token.token)